﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Interfaces;
using UnityEngine.EventSystems;
using Utility;
using Services;

public class Character : MonoBehaviour, IKillable
{
    [SerializeField]
    Transform abilityPosition;
    [SerializeField]
    float cooldown = 2;
    [SerializeField]
    GameObject abilityPrefab;

    List<KeyValuePair<GameObject, IInteractableObject>> nearbyObjects = new List<KeyValuePair<GameObject, IInteractableObject>>();
    GameObject activeTimeZone;
    CharacterMovement movement;
    bool abilityReady = true;
    private LevelManager _levelManager;
    private UI _ui;

    void Awake()
    {
        movement = GetComponent<CharacterMovement>();
    }

    void Start()
    {
        var gui = Managers.Get("GUI");
        _levelManager = Managers.Get<LevelManager>("LevelManager");
        _ui = gui.GetComponentInChildren<UI>();
        _ui.Register(this);
    }

    public void Kill()
    {
        _levelManager.RestartLevel();
    }

    public void UseAbility()
    {
        Destroy(activeTimeZone);
        activeTimeZone = Instantiate<GameObject>(abilityPrefab);
        activeTimeZone.transform.position = abilityPosition.position;
        abilityReady = false;
        new Timer(cooldown, () => abilityReady = true);
    }

    public void SetDirection(MoveDirection moveDirection)
    {
        movement.SetDirection(moveDirection);
    }

    public void UseClosestObject()
    {
        if (!nearbyObjects.Any()) return;

        var closestObject = nearbyObjects
            .OrderByDescending(kvp => Vector3.Distance(kvp.Key.transform.position, transform.position))
            .Select(kvp => kvp.Value)
            .FirstOrDefault();

        if(closestObject != null)
        {
            closestObject.Interact();
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var component = collider.GetComponent<IInteractableObject>();
        if (component != null)
        {
            var kvp = new KeyValuePair<GameObject, IInteractableObject>(collider.gameObject, component);
            nearbyObjects.Add(kvp);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        var inCollection = nearbyObjects
            .Select(kvp => kvp.Key)
            .Contains(collider.gameObject);

        if (inCollection)
        {
            var index = nearbyObjects.FindIndex(kvp => kvp.Key == collider.gameObject);
            nearbyObjects.RemoveAt(index);
        }
    }
}
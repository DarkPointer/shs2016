﻿using Interfaces;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Utility;
using System;

public class TimeZone : MonoBehaviour
{
    private IDictionary<GameObject, ITimeModifiable> _modifiedObjects = new Dictionary<GameObject, ITimeModifiable>();
    [SerializeField]
    private float _factor = 1;
    [SerializeField]
    private float _radius = 5;

    void Awake()
    {
        var nearbyObjects = FindNearbyObjects();

        foreach (var kvp in nearbyObjects)
        {
            kvp.Value.AddModification(this, _factor);
            _modifiedObjects.Add(kvp);
        }

        if (Boot.DEBUG_LEVEL >= DebugLevel.Debug)
        {
            LogHelper.Log("Found {0} objects", _modifiedObjects.Count);
        }
    }

    IDictionary<GameObject, ITimeModifiable> FindNearbyObjects()
    {
        return Physics2D.OverlapCircleAll(transform.position, _radius)
            .Select(c => new KeyValuePair<GameObject, ITimeModifiable>(c.gameObject, c.GetComponent<ITimeModifiable>()))
            .Where(kvp => kvp.Value != null)
            .Distinct()
            .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
    }

    void OnDestroy()
    {
        foreach (var obj in _modifiedObjects)
        {
            obj.Value.RemoveModification(this, _factor);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var component = collider.GetComponent<ITimeModifiable>();
        if (component != null)
        {
            if (!_modifiedObjects.ContainsKey(collider.gameObject))
            {
                component.AddModification(this, _factor);
                _modifiedObjects.Add(collider.gameObject, component);
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        var component = collider.GetComponent<ITimeModifiable>();
        if (component != null)
        {
            if (_modifiedObjects.ContainsKey(collider.gameObject))
            {
                component.RemoveModification(this, _factor);
                _modifiedObjects.Remove(collider.gameObject);
            }
        }
    }

    void OnDrawGizmos()
    {
        if (Boot.GIZMOS_DEBUG_LEVEL == GizmosDebugLevel.Always)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, _radius);
        }
    }
    void OnDrawGizmosSelected()
    {
        if (Boot.GIZMOS_DEBUG_LEVEL >= GizmosDebugLevel.Selected)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, _radius);
        }
    }
}

﻿using UnityEngine;
using Interfaces;
using System;

public class Flower : MonoBehaviour, ITimeModifiable
{
    private float scale = 1.1f;
    private float currentTimeFactor = 1;

    void Start()
    {
        // Start coroutine here
    }

    public void AddModification(object blocker, float factor)
    {
        currentTimeFactor *= factor;
    }

    public void RemoveModification(object blocker, float factor)
    {
        currentTimeFactor /= factor;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            Grow();
        }
    }

    void Grow()
    {
        transform.localScale *= scale;
        if (transform.localScale.x >= 5.0F)
        {
            transform.localScale = new Vector3(5.0F, 5.0F, 5.0F);
        }
    }
}

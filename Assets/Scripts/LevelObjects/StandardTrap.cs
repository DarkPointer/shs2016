﻿using Interfaces;
using UnityEngine;

public class StandardTrap : MonoBehaviour, ISetActiveState
{
    public bool isActive;
    [SerializeField]
    private int Damage;

    public void SetActiveState(bool newState)
    {
        isActive = newState;
    }

    void OnTriggerEnter2D(Collider2D Player)
    {
        if (isActive == true)
        {
            var playerComponent = Player.GetComponent<Character>();
            if (playerComponent)
            {
                playerComponent.Kill();
            }
            isActive = false;
        }
    }
}

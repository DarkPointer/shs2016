﻿using Interfaces;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LevelObjects
{
    public class RotatingBridge : MonoBehaviour, ITimeModifiable
    {
        private float rotationSpeed = 1;
        private float currentTimeFactor = 1;
        private List<KeyValuePair<object, float>> timeModifications = new List<KeyValuePair<object, float>>();

        public void AddModification(object blocker, float factor)
        {
            timeModifications.Add(new KeyValuePair<object, float>(blocker, factor));
            currentTimeFactor = CurrentTimeFactor();
        }

        public void RemoveModification(object blocker, float factor)
        {
            // Todo: Find more elegant solution
            var index = timeModifications
                .FindIndex(kvp => kvp.Key == blocker);

            timeModifications.RemoveAt(index);
            currentTimeFactor = CurrentTimeFactor();
        }

        private float CurrentTimeFactor()
        {
            return 1 - timeModifications
                .Select(kvp => 1 - kvp.Value)
                .Sum();
        }

        void Update()
        {
            transform.Rotate(0, 0, rotationSpeed * currentTimeFactor);
        }
    }
}

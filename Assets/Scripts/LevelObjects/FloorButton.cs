﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility;

namespace LevelObjects
{
    public class FloorButton : MonoBehaviour, ITimeModifiable, IInteractableObject
    {
        [SerializeField]
        private GameObject[] objectsToDisable;
        [SerializeField]
        private float objectsDisabledForSeconds;
        private ISetActiveState[] objects;
        private bool buttonEnabled = true;

        private float currentTimeFactor = 1;

        void Awake()
        {
            objects = objectsToDisable
                .Select(o => o.GetComponent<ISetActiveState>())
                .Where(o => o != null)
                .ToArray();
        }

        public void AddModification(object blocker, float factor)
        {
            currentTimeFactor *= factor;
        }

        public void RemoveModification(object blocker, float factor)
        {
            currentTimeFactor /= factor;
        }

        public void Interact()
        {
            Debug.Log(DateTime.Now);
            if(buttonEnabled)
            {
                foreach (var o in objects)
                {
                    o.SetActiveState(false);
                }
                new Timer(objectsDisabledForSeconds, EnableObjects, () => currentTimeFactor);
            }
            buttonEnabled = false;
        }

        private void EnableObjects()
        {
            Debug.Log(DateTime.Now);
            foreach (var o in objects)
            {
                o.SetActiveState(true);
            }
            buttonEnabled = true;
        }
    }
}

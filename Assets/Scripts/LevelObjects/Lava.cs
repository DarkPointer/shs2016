﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.LevelObjects
{
    public class Lava : MonoBehaviour
    {
        void OnTriggerEnter2D(Collider2D collider)
        {
            var component = collider.GetComponent<IKillable>();
            if (component != null)
            {
                component.Kill();
            }
        }
    }
}

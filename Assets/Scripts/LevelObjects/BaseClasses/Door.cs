﻿using System;
using Interfaces;
using UnityEngine;

namespace LevelObjects.BaseClasses
{
    public abstract class Door : MonoBehaviour, IInteractableObject
    {
        public abstract void Interact();
    }
}

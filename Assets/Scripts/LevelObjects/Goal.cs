﻿using UnityEngine;
using System.Collections.Generic;
using Services;
using System;
using System.Collections;

public class Goal : MonoBehaviour
{
    List<Character> characters = new List<Character>();
    private LevelManager _levelManager;
    
    void Start()
    {
        _levelManager = Managers.Get<LevelManager>("LevelManager");
        StartCoroutine(TrackPlayerCount());
    }

    private IEnumerator TrackPlayerCount()
    {
        while (characters.Count < 2)
        {
            yield return null;
        }
        _levelManager.LoadNextScene();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var component = collider.GetComponent<Character>();
        if (component)
        {
            characters.Add(component);
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        var component = collider.GetComponent<Character>();
        if (component)
        {
            characters.Remove(component);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using Interfaces;
using Services;

public class Turret : MonoBehaviour, ITimeModifiable
{
    public Projectile projectilePrefab;
    public GameObject muzzle;
    private LevelManager _levelManager;

    [SerializeField]
    private float timeBetweenShots = 2;
    
    void Start()
    {
        _levelManager = Managers.Get<LevelManager>("LevelManager");
        _levelManager.OnLevelRestart += OnLevelRestart;
        StartCoroutine(Shoot());
    }

    void OnDestroy()
    {
        if(_levelManager != null)
        {
            _levelManager.OnLevelRestart -= OnLevelRestart;
        }
    }

    IEnumerator Shoot()
    {
        while (true)
        {
            ShootProjectile();
            yield return new WaitForSeconds(timeBetweenShots);
        }
    }

    void OnLevelRestart()
    {
        StopAllCoroutines();
    }

    void ShootProjectile()
    {
        var newobject = Instantiate<Projectile>(projectilePrefab);
        newobject.transform.position = muzzle.transform.position;
        newobject.SetDirection(muzzle.transform.position - transform.position);
    }

    public void AddModification(object blocker, float factor)
    {
        timeBetweenShots /= factor;
    }

    public void RemoveModification(object blocker, float factor)
    {
        timeBetweenShots *= factor;
    }
}

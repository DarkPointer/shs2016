﻿namespace Interfaces
{
    public interface IInteractableObject
    {
        void Interact();
    }
}
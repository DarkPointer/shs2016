﻿namespace Interfaces
{
    public interface IPlayerControls
    {
        void Move(UnityEngine.Vector2 direction);
        void UseAbility();
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using Utility;
using Services;

public class UI : MonoBehaviour
{
    public int[] hiddenOnLevels;

    private LinkedList<MoveDirection> moveStack = new LinkedList<MoveDirection>();

    private int time = 0;
    [SerializeField]
    private int levelTime = 100;
    private int timeLeft = 100;
    public Text timeText;

    private DebugLevel requiredDebugLevel = DebugLevel.All;

    public AudioSource switcher;
    public AudioSource power;
    List<Character> characters = new List<Character>();
    int activeCharacter = 0;
    private LevelManager _levelManager;

    void Start()
    {
        _levelManager = Managers.Get<LevelManager>("LevelManager");
        _levelManager.OnLevelRestart += OnPlayerDeath;
        StartCoroutine(Timer());
    }

    void Update()
    {
        characters.RemoveAll(c => c == null);
        if (ActiveCharacter == null) return;

        ActiveCharacter.SetDirection(moveStack.Any()
            ? moveStack.First()
            : MoveDirection.None);
    }

    void FixedUpdate()
    {
        timeText.text = "Time: " + timeLeft.ToString();
    }

    void OnPlayerDeath()
    {
        moveStack.Clear();
        timeLeft = levelTime;
    }

    IEnumerator Timer()
    {
        while (timeLeft > 0)
        {
            yield return new WaitForSeconds(1);
            timeLeft -= 1;
        }
        GameOver();
    }

    public Character ActiveCharacter
    {
        get
        {
            return characters.Any()
                ? characters[activeCharacter]
                : null;
        }
    }

    public void Register(Character character)
    {
        characters.Add(character);
    }

    public void Deregister(Character character)
    {
        characters.Remove(character);
    }

    void GameOver()
    {
        Debug.Log("GAMEOVER");
    }

    public void HoldingUp()
    {
        LogHelper.Log("Up", requiredDebugLevel);
        moveStack.AddFirst(MoveDirection.Up);
    }

    public void ReleaseUp()
    {
       
        LogHelper.Log("Released up", requiredDebugLevel);
        moveStack.Remove(MoveDirection.Up);
    }

    public void HoldingDown()
    {
        LogHelper.Log("Down", requiredDebugLevel);
        moveStack.AddFirst(MoveDirection.Down);
    }

    public void ReleaseDown()
    {
        LogHelper.Log("Released down", requiredDebugLevel);
        moveStack.Remove(MoveDirection.Down);
    }

    public void HoldingLeft()
    {
        LogHelper.Log("Left", requiredDebugLevel);
        moveStack.AddFirst(MoveDirection.Left);
    }

    public void ReleaseLeft()
    {
        LogHelper.Log("Released left", requiredDebugLevel);
        moveStack.Remove(MoveDirection.Left);
    }

    public void HoldingRight()
    {
        LogHelper.Log("Right", requiredDebugLevel);
        moveStack.AddFirst(MoveDirection.Right);
    }

    public void ReleaseRight()
    {
        LogHelper.Log("Released right", requiredDebugLevel);
        moveStack.Remove(MoveDirection.Right);
    }

    public void SwitchCharacter()
    {
        if (switcher != null)
        {
            switcher.Play();
        }
        if (ActiveCharacter != null)
        {
            ActiveCharacter.SetDirection(MoveDirection.None);
            activeCharacter = (activeCharacter == 0)
                ? 1
                : 0;
        }
    }

    public void UsePowerOfCurrentCharacter()
    {
        if (ActiveCharacter != null)
        {
            if (power != null)
            {
                power.Play();
            }
            ActiveCharacter.UseAbility();
        }
    }

    public void InteractWithThings()
    {
        ActiveCharacter.UseClosestObject();
    }

}

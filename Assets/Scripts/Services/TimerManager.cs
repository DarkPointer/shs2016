﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility;

namespace Services
{
    public class TimerManager : MonoBehaviour
    {
        private List<Timer> timers = new List<Timer>();

        void Update()
        {
            for (int i = 0; i < timers.Count; i++)
            {
                var timer = timers[i];
                timer.AddElapsedTime(Time.deltaTime);
            }
            var completeTimers = timers.Where(t => t.Completed);
            foreach (var timer in completeTimers)
            {
                timer.Finished();
            }

            timers.RemoveAll(t => t.Completed);
        }

        public void Add(Timer timer)
        {
            timers.Add(timer);
        }

        public void Remove(Timer timer)
        {
            timers.Remove(timer);
        }
    }
}

﻿using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services
{
    public class GUIManager : MonoBehaviour
    {
        private GameObject _gui;
        private UI _ui;

        void Awake()
        {
            _gui = Managers.Get("GUI");
            _ui = _gui.GetComponentInChildren<UI>();
            OnLevelWasLoadedLogic();
        }

        void OnLevelWasLoaded()
        {
            OnLevelWasLoadedLogic();
        }

        void Update()
        {
            _gui.transform.position = Vector3.zero;
        }

        private void OnLevelWasLoadedLogic()
        {
            var index = SceneManager.GetActiveScene().buildIndex;

            if (_ui.hiddenOnLevels.Contains(index) || index == SceneManager.sceneCount)
            {
                _gui.SetActive(false);
            }
            else
            {
                _gui.SetActive(true);
            }
        }
    }
}

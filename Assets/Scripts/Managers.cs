﻿using Utility;
using System.Collections.Generic;
using UnityEngine;

public class Managers
{
    private static IDictionary<string, GameObject> _managers = new Dictionary<string, GameObject>();

    public static void Register(string key, GameObject manager)
    {
        _managers.Add(key, manager);
        if(Boot.DEBUG_LEVEL >= DebugLevel.Debug)
        {
            LogHelper.Log("Recieved key: {0}", key);
        }
    }

    public static TManager Get<TManager>(string key) where TManager : MonoBehaviour
    {
        GameObject manager;
        return _managers.TryGetValue(key.Trim(), out manager)
            ? manager.GetComponent<TManager>()
            : null;
    }
    
    public static GameObject Get(string key)
    {
        GameObject manager;
        return _managers.TryGetValue(key.Trim(), out manager)
            ? manager
            : null;
    }
}
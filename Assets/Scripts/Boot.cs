﻿using Utility;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum DebugLevel
{
    None,
    Debug,
    All
}

public enum GizmosDebugLevel
{
    None,
    Selected,
    Always
}

[Serializable]
public struct ManagerStruct
{
    public string Key;
    public GameObject Prefab;
}

public class Boot : MonoBehaviour
{
    public static DebugLevel DEBUG_LEVEL = DebugLevel.None;
    public static GizmosDebugLevel GIZMOS_DEBUG_LEVEL = GizmosDebugLevel.None;

    private static bool managersExist = false;

    [SerializeField]
    private DebugLevel DebugLevel;
    [SerializeField]
    private GizmosDebugLevel GizmosDebug;
    [SerializeField]
    private List<ManagerStruct> _managers = new List<ManagerStruct>();
    [SerializeField]
    private bool _stopGameOnException = false;
    [SerializeField]
    private bool _loadTestLevel = false;
    [SerializeField]
    private string _levelToLoad = "";
    [SerializeField]
    private int targetFrameRate = 30;

    private void Awake()
    {
        Application.targetFrameRate = targetFrameRate;
        if (managersExist)
        {
            Destroy(gameObject);
            return;
        }
        DEBUG_LEVEL = DebugLevel;
        GIZMOS_DEBUG_LEVEL = GizmosDebug;

        AddCustomLogHandling();
        if (CreateManagers(Managers.Register, _managers))
        {
            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                if (string.IsNullOrEmpty(_levelToLoad))
                {
                    if (_loadTestLevel)
                    {
                        SceneManager.LoadScene("TestScene");
                    }
                    else
                    {
                        SceneManager.LoadScene(1);
                    }
                }
                else
                {
                    int level;
                    if (int.TryParse(_levelToLoad, out level))
                    {
                        SceneManager.LoadScene(level);
                    }
                    SceneManager.LoadScene(_levelToLoad);
                }
            }
        }
    }

    private void AddCustomLogHandling()
    {
        if (_stopGameOnException)
        {
#if UNITY_5
            Application.logMessageReceived += HandleLog;
#else
            Application.RegisterLogCallback(HandleLog);
#endif
        }
    }

    private void HandleLog(string condition, string stackTrace, LogType type)
    {
        if (type == LogType.Exception)
        {
            Time.timeScale = 0;
        }
    }

    private bool CreateManagers(Action<string, GameObject> managerRegistration, IEnumerable<ManagerStruct> managers)
    {
        var managerObject = new GameObject("Managers");
        DontDestroyOnLoad(managerObject);
        Vector3 managerPosition = new Vector3(10000, 10000);

        foreach (var kvp in managers)
        {
            if (string.IsNullOrEmpty(kvp.Key.Trim()))
            {
                throw new ArgumentNullException("Invalid key");
            }
            if (!kvp.Prefab)
            {
                throw new ArgumentNullException(string.Format("Null prefab for key {0}", kvp.Key));
            }
            if (DEBUG_LEVEL >= DebugLevel.Debug)
            {
                LogHelper.Log("Spawned manager for key: {0}", kvp.Key);
            }
            var manager = Instantiate(kvp.Prefab);
            manager.transform.position = managerPosition;
            manager.transform.SetParent(managerObject.transform);
            managerRegistration(kvp.Key, manager);
        }
        managersExist = true;
        return true;
    }
    void OnValidate()
    {
        if (!string.IsNullOrEmpty(_levelToLoad))
        {
            _loadTestLevel = false;
        }
    }
}
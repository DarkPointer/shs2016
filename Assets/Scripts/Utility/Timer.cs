﻿using Services;
using System;
using UnityEngine;

namespace Utility
{
    public class Timer
    {
        private Action _onFinished;
        private Func<float> _getTimeFactor;
        private float _elapsedModifiedTime = 0;
        private float _doneAfter;
        private TimerManager _timerManager;

        public Timer(float seconds, Action onFinished, Func<float> getTimeFactor = null, bool registerInManager = true)
        {
            _doneAfter = seconds;
            _onFinished = onFinished;
            _timerManager = Managers.Get<TimerManager>("TimerManager");
            if (registerInManager)
            {
                _timerManager.Add(this);
            }
            if (getTimeFactor == null)
            {
                _getTimeFactor = () => 1;
            }
            else
            {
                _getTimeFactor = getTimeFactor;
            }
        }

        public void AddElapsedTime(float deltaTime)
        {
            var time = deltaTime * _getTimeFactor();
            _elapsedModifiedTime += time;
        }

        public bool Completed
        {
            get
            {
                return _elapsedModifiedTime >= _doneAfter;
            }
        }

        public float TimeLeft
        {
            get
            {
                return _doneAfter - _elapsedModifiedTime;
            }
        }

        public void Finished()
        {
            _onFinished();
        }

        public void Cancel()
        {
            _timerManager.Remove(this);
        }
    }
}

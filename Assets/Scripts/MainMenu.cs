﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Services;

public class MainMenu : MonoBehaviour {

    public Canvas credits;
    public Canvas story;

	// Use this for initialization
	void Start ()
    {
        credits.enabled = false;
        story.enabled = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void StartGame()
    {
        Managers.Get<LevelManager>("LevelManager").LoadNextScene();
    }

    public void ShowStory()
    {
        story.enabled = true;
    }

    public void HideStory()
    {
        story.enabled = false;
    }

    public void ShowCredits()
    {
        credits.enabled = true;
    }

    public void HideCredits()
    {
        credits.enabled = false;
    }
}

﻿using Interfaces;
using Utility;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
    public class TestObject : MonoBehaviour, ITimeModifiable
    {
        private IDictionary<object, float> _modifications = new Dictionary<object, float>();

        public void AddModification(object blocker, float factor)
        {
            _modifications.Add(blocker, factor);
            LogHelper.Log("Recieved modification with factor {0}", factor);
        }

        public void RemoveModification(object blocker, float factor)
        {
            float _factor;
            _modifications.TryGetValue(blocker, out _factor);
            _modifications.Remove(blocker);
            LogHelper.Log("Removed object with factor {0}", _factor);
        }
    }
}
